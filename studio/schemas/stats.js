export default {
  __experimental_actions: ['update', 'publish'] /* 'create', 'delete' */,
  liveEdit: false,
  fields: [
    {
      name: 'title',
      title: 'Page title',
      type: 'string'
    },
    {
      name: 'image',
      title: 'Stats image (optional)',
      type: 'figure'
    },
    {
      name: 'body',
      title: 'Stats body',
      type: 'blockContent'
    },
    {
      name: 'listTitle',
      title: 'List title',
      type: 'string'
    },
    {
      name: 'listItems',
      title: 'List items',
      of: [
        {
          type: 'listItems'
        }
      ],
      type: 'array'
    }
  ],
  title: 'Stats Page',
  type: 'document',
  name: 'statsPage'
}

export default {
  __experimental_actions: ['update', 'publish'] /* 'create', 'delete' */,
  liveEdit: false,
  fields: [
    {
      name: 'title',
      title: 'Blog Page title',
      type: 'string'
    },
    {
      name: 'description',
      title: 'Blog Page SEO Description',
      type: 'text'
    },
    {
      name: 'body',
      title: 'Blog Page body',
      type: 'blockContent'
    },
    {
      name: 'tabs',
      title: 'Show blog and topics tabs',
      type: 'boolean'
    }
  ],
  title: 'Blog Page',
  type: 'document',
  name: 'blogPage'
}

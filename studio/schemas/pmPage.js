export default {
  __experimental_actions: ['update', 'publish', 'create'] /* 'create', 'delete' */,
  liveEdit: false,
  fields: [
    {
      name: 'title',
      title: 'Page title',
      type: 'string'
    },
    {
      name: 'image',
      title: 'PM image',
      type: 'figure'
    },
    {
      name: 'description',
      title: 'PM SEO Description',
      type: 'text'
    },
    {
      name: 'body',
      title: 'PM body',
      type: 'blockContent'
    }
  ],
  title: 'Product Managers Page',
  type: 'document',
  name: 'pmPage'
}

export default {
  __experimental_actions: ['update', 'publish'] /* 'create', 'delete' */,
  liveEdit: false,
  fields: [
    {
      name: 'title',
      title: 'Page title',
      type: 'string'
    },
    {
      name: 'image',
      title: 'Sales image',
      type: 'figure'
    },
    {
      name: 'body',
      title: 'Sales body',
      type: 'blockContent'
    }
  ],
  title: 'Sales Page',
  type: 'document',
  name: 'salesPage'
}

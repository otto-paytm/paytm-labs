export default {
  __experimental_actions: ['update', 'publish'] /* 'create', 'delete' */,
  liveEdit: false,
  fields: [
    {
      name: 'title',
      title: 'Page title',
      type: 'string'
    },
    {
      name: 'image',
      title: 'Product page image',
      type: 'figure'
    },
    {
      name: 'body',
      title: 'Product page body',
      type: 'blockContent'
    }
  ],
  title: 'Product Page',
  type: 'document',
  name: 'productPage'
}

import { MdFormatQuote } from 'react-icons/md'

export default {
  name: 'quote',
  title: 'Quote',
  type: 'document',
  icon: MdFormatQuote,
  liveEdit: false,
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string'
    },
    {
      name: 'image',
      title: 'Image',
      type: 'image'
    },
    {
      name: 'description',
      type: 'text',
      title: 'Description',
      description: 'Description of the list item'
    },
    {
      name: 'link',
      type: 'url',
      title: 'Link (optional)',
      description: 'If you add a link the list item the frontend will add a link'
    }
  ],
  preview: {
    select: {
      title: 'title',
      media: 'image'
    }
  }
}

export default {
  __experimental_actions: ['update', 'publish'] /* 'create', 'delete' */,
  liveEdit: false,
  fields: [
    {
      name: 'title',
      title: 'Page title',
      type: 'string'
    },
    {
      name: 'image',
      title: 'Security image',
      type: 'figure'
    },
    {
      name: 'body',
      title: 'Security body',
      type: 'blockContent'
    },
    {
      name: 'listTitle',
      title: 'Stats title',
      type: 'string'
    },
    {
      name: 'listItems',
      title: 'Stats items',
      of: [
        {
          type: 'listItems'
        }
      ],
      type: 'array'
    },
    {
      name: 'title2',
      title: 'Security title',
      type: 'string'
    },
    {
      name: 'body2',
      title: 'Security body',
      type: 'blockContent'
    }
  ],
  title: 'Security Page',
  type: 'document',
  name: 'securityPage'
}

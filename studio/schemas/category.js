import { MdApps } from 'react-icons/md'

export default {
  name: 'category',
  title: 'Category',
  type: 'document',
  __experimental_actions: ['update', 'publish', 'create', 'delete'],
  icon: MdApps,
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string'
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      description: 'The URL this post will have, ie: paytmlabs.com/topics/title-of-category',
      options: {
        source: 'title',
        maxLength: 96
      }
    },
    {
      name: 'description',
      title: 'Description',
      type: 'text'
    }
  ],
  liveEdit: false
}

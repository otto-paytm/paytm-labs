require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`
})

module.exports = {
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-postcss',
    'gatsby-plugin-netlify',
    {
      resolve: 'gatsby-plugin-sass',
      options: {
        implementation: require('node-sass')
      }
    },
    {
      resolve: 'gatsby-plugin-gtag',
      trackingId: process.env.GATSBY_GA_ID,
      // Setting this parameter is optional
      anonymize: true
    },
    {
      // keep as first gatsby-source-filesystem plugin for gatsby image support
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/static/`,
        name: 'uploads'
      }
    },
    {
      resolve: 'gatsby-source-sanity',
      options: {
        // To enable preview of drafts, copy .env-example into .env,
        // and add a token with read permissions
        overlayDrafts: true,
        watchMode: true,
        // Config
        projectId: process.env.SANITY_PROJECT_ID,
        dataset: process.env.SANITY_DATASET,
        token: process.env.SANITY_TOKEN
      }
    }
  ]
}

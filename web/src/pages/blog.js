import React, { useState } from 'react'
import { graphql, Link } from 'gatsby'
import Pagination from 'react-sanity-pagination'

import {
  filterOutDocsPublishedInTheFuture,
  filterOutDocsWithoutSlugs,
  mapEdgesToNodes
} from '../lib/helpers'

import BlogPostPreviewGrid from '../components/blog-post-preview-grid'
import GraphQLErrorList from '../components/graphql-error-list'
import BlockContent from '../components/block-content'
import SEO from '../components/seo'

import Layout from '../containers/layout'

const ParagraphClass = 'dib relative w-100 measure-wide gray fw4 mb4'
const HeadingClass = 'dib relative w-100 measure-wide f2 b mv3 black'
const TabClass = 'dib relative w-auto b f7 ttu brand-navy bb bw1 mr3 mr4-ns link dim pv2 tracked'

export const query = graphql`
  query BlogPageQuery {
    blog: sanityBlogPage(_id: { regex: "/(drafts.|)blogPage/" }) {
      title
      description
      _rawBody
      tabs
    }

    posts: allSanityPost(sort: { fields: [publishedAt], order: DESC }) {
      edges {
        node {
          id
          publishedAt
          mainImage {
            asset {
              _id
            }
            alt
          }
          categories {
            title
            description
          }
          title
          description
          slug {
            current
          }
        }
      }
    }
  }
`
const BlogPage = props => {
  const { data, errors } = props

  const blog = (data || {}).blog

  if (errors) {
    return (
      <Layout>
        <GraphQLErrorList errors={errors} />
      </Layout>
    )
  }

  // Create a variable for the amount of posts you want per page
  const postsPerPage = 12
  // Create state which will be updated every time you paginate
  const [items, setItems] = useState([])

  // Fetch all data initially
  const postNodes = (data || {}).posts
    ? mapEdgesToNodes(data.posts)
        .filter(filterOutDocsWithoutSlugs)
        .filter(filterOutDocsPublishedInTheFuture)
    : []

  // Create an action which will be called on paginate
  // This will return the current Page, Range of items and the Items to render
  const action = (page, range, items) => {
    // Update State
    setItems(items)
  }

  return (
    <Layout>
      <SEO title={blog.title || 'Blog'} description={blog.description || 'Paytm Labs Blog'} />
      <article>
        <section className="dib relative w-100 lh-body dark-gray f5">
          <div className="db center relative mw8 ph4 pv6 z-1">
            <h1 className={HeadingClass}>{blog.title || 'Blog'}</h1>
            {blog._rawBody ? (
              <div className={ParagraphClass}>
                <BlockContent blocks={blog._rawBody || {}} />
              </div>
            ) : null}
            {blog.tabs ? (
              <div className="dib relative w-100 overflow-x-auto mb4 nowrap">
                <Link className={`${TabClass} b--brand-navy`} to="/blog">
                  Latest posts
                </Link>
                <Link className={`${TabClass} b--white`} to="/topics">
                  Topics
                </Link>
              </div>
            ) : null}
            {items ? (
              <BlogPostPreviewGrid nodes={items} />
            ) : (
              <div className="dib relative w-100 tc">
                <img className="db center mw5 pa4" alt="Paytm Labs" src="/img/logo.svg" />
                <h1 className={HeadingClass}>No blog posts found</h1>
                <p className={ParagraphClass}>
                  Oh no... we don't have any content on this page right now. Check back later!
                </p>
              </div>
            )}
            {/* Props required: action, items, postsPerPage */}
            <Pagination
              nextButtonLabel={'Next'}
              prevButtonLabel={'Prev'}
              postsPerPage={postsPerPage}
              action={action}
              items={postNodes}
              nextButton
              prevButton
            />
          </div>
        </section>
      </article>
    </Layout>
  )
}

export default BlogPage

import React from 'react'
import { graphql, Link } from 'gatsby'

import { filterOutDocsWithoutSlugs, mapEdgesToNodes } from '../lib/helpers'

import GraphQLErrorList from '../components/graphql-error-list'
import SEO from '../components/seo'

import Layout from '../containers/layout'

const ParagraphClass = 'dib relative w-100 measure-wide gray fw4 mb4'
const HeadingClass = 'dib relative w-100 measure-wide f2 b mv3 black'
const TabClass = 'dib relative w-auto b f7 ttu black bb bw1 mr4 link dim pv2 tracked'

export const query = graphql`
  query TopicsPageQuery {
    categories: allSanityCategory(sort: { fields: [title], order: ASC }) {
      edges {
        node {
          id
          title
          slug {
            current
          }
        }
      }
    }
  }
`

const TopicsPage = props => {
  const { data, errors } = props

  const categories = (data || {}).categories
    ? mapEdgesToNodes(data.categories).filter(filterOutDocsWithoutSlugs)
    : []

  if (errors) {
    return (
      <Layout>
        <GraphQLErrorList errors={errors} />
      </Layout>
    )
  }

  return (
    <Layout>
      <SEO title="Blog Topics" />
      <article>
        <section className="dib relative w-100 lh-body dark-gray f5">
          <div className="db center mw8 ph4 pv6">
            <h1 className={HeadingClass}>Topics</h1>
            <div className="dib relative w-100 overflow-x-auto mb4 nowrap">
              <Link className={`${TabClass} b--white`} to="/blog">
                Latest posts
              </Link>
              <Link className={`${TabClass} b--black`} to="/topics">
                Topics
              </Link>
            </div>
            {categories ? (
              <div className="dib relative w-100">
                <div className="row">
                  {categories.map((object, index) => (
                    <div className="col-xs-12 col-sm-6" key={`topic-${index}`}>
                      <Link
                        className="dib relative w-100 link dim f3 b black mb4"
                        to={`/topics/${object.slug.current}`}
                      >
                        {object.title}
                      </Link>
                    </div>
                  ))}
                </div>
              </div>
            ) : (
              <div className="dib relative w-100 tc">
                <img className="db center mw5 pa4" alt="Paytm Labs" src="/img/logo.svg" />
                <h1 className={HeadingClass}>Not topics found</h1>
                <p className={ParagraphClass}>
                  Oh no... we don't have any content on this page right now. Check back later!
                </p>
              </div>
            )}
          </div>
        </section>
      </article>
    </Layout>
  )
}

export default TopicsPage

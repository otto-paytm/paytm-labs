import React from 'react'

import ButtonLearnMore from '../components/button-learn'
import SEO from '../components/seo'

import Layout from '../containers/layout'

const ParagraphClass = 'dib relative w-100 measure-wide gray fw4 mb4'
const HeadingClass = 'dib relative w-100 f2 b mt0 mb2'

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />
    <article>
      <section className="dt relative vh-100 w-100 bg-white black lh-body f5" id="home">
        <div className="dtc v-mid w-100 relative tc">
          <div className="dib relative w-100">
            <img className="db center mw5 pa4" alt="Paytm Logo" src="/img/logo.svg" />
            <h1 className={HeadingClass}>Nothing here...</h1>
            <p className={ParagraphClass}>You've stumbled upon a path that doesn't exists</p>
          </div>
          <ButtonLearnMore text="Go home" to="/" />
        </div>
      </section>
    </article>
  </Layout>
)

export default NotFoundPage

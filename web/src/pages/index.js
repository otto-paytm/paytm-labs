import React from 'react'
import axios from 'axios'
import { graphql } from 'gatsby'
import AnchorLink from 'react-anchor-link-smooth-scroll'
import ReadMoreReact from 'read-more-react'

// Lib
import { buildImageObj } from '../lib/helpers'
import { imageUrlFor } from '../lib/image-url'

// Components
import GraphQLErrorList from '../components/graphql-error-list'
import BlockContent from '../components/block-content'
import JobOpening from '../components/job-opening'
import Banner from '../components/banner'
import SEO from '../components/seo'
import HR from '../components/hr'

// Containers
import Layout from '../containers/layout'

export const query = graphql`
  query IndexPageQuery {
    site: sanitySiteSettings(_id: { regex: "/(drafts.|)siteSettings/" }) {
      title
      description
      keywords
    }

    home: sanityHomePage(_id: { regex: "/(drafts.|)homePage/" }) {
      title
      banner
      bannerText
      bannerDescription
      bannetCTA
      bannerCTALink
      bannerCTAExternal
      image {
        asset {
          _id
        }
      }
      _rawBody
      story
      storyBody
      values
      valuesBody
      valueLI {
        title
        description
        icon {
          asset {
            _id
          }
        }
      }
      equality
      equalityBody
      equalityImage {
        asset {
          _id
        }
      }
      benefits
      showBlue
      benefitsBlue
      benefitsLI {
        title
        description
        icon {
          asset {
            _id
          }
        }
      }
      benefitsImage {
        asset {
          _id
        }
      }
      careers
      careersBody
      showGPTW
      gptwLink
      gptwBody
      careersCTA
      careersLink
      careersThanks
    }
  }
`

// Classes
const SubHeadingClass = 'dib relative w-100 measure-wide f2 b mv3'
const ParagraphClass = 'dib relative w-100 measure-wide inherit fw4 mb4'
const HeadingClass2 = 'dib relative w-100 measure-wide f2 f1-l b brand-navy lh-copy'
const HeadingClass = 'dib relative w-100 measure-wide f2 f1-l b brand-navy lh-copy ma0'

class IndexPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hasError: false,
      loaded: false,
      jobs: []
    }
  }

  getLever() {
    axios
      .get('https://api.lever.co/v0/postings/paytm', {
        params: {
          location: 'Toronto, Canada',
          limit: 12,
          mode: 'json'
        }
      })
      .then(response => {
        this.setState(state => {
          const jobs = state.jobs.concat(response.data)

          return {
            jobs
          }
        })
      })
      .catch(error => {
        console.log(error)
        this.setState({
          hasError: true
        })
      })
  }

  componentDidMount() {
    this.getLever()

    this.setState({
      loaded: true
    })
  }

  render() {
    const { data, errors } = this.props
    const { loaded, jobs } = this.state

    const site = (data || {}).site
    const home = (data || {}).home

    const ListItem = ({ object }) => (
      <div className="flex justify-between w-100 top-xs pv2">
        {object.icon && object.icon.asset && object.icon.asset._id ? (
          <span className="w2 h2 br-pill bg-transparent pa1 mr4">
            <img
              className="dib relative w-100 h-100 fit"
              src={imageUrlFor(buildImageObj(object.icon))}
              alt={object.title}
            />
          </span>
        ) : null}
        <div className="dib relative w-100">
          <strong className="dib relative w-100 brand-navy f4 b lh-copy">{object.title}</strong>
          {object.description ? <p className={ParagraphClass}>{object.description}</p> : null}
        </div>
      </div>
    )

    const homeAssetID = home.image && home.image.asset && home.image.asset._id
    const homeImage = homeAssetID
      ? imageUrlFor(buildImageObj(home.image))
      : 'https://images.unsplash.com/photo-1461701204332-2aa3db5b20c8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2250&q=80'

    const banner = {
      description: home.bannerDescription || false,
      external: home.bannerCTAExternal || false,
      ctaLink: home.bannerCTALink || false,
      ctaText: home.bannetCTA || false,
      text: home.bannerText || false,
      show: home.banner || false
    }

    if (!site) {
      throw new Error(
        'Missing "Site Settings" data. Open the studio at http://localhost:3333 and add "Site Settings" page data and restart the development server.'
      )
    }

    if (errors) {
      return (
        <Layout>
          <GraphQLErrorList errors={errors} />
        </Layout>
      )
    } else {
      return (
        <Layout home>
          <SEO title={site.title} description={site.description} keywords={site.keywords} />
          <article>
            {/* Banner */}
            {banner && banner.show ? (
              <Banner data={banner} />
            ) : null}
            {/* Home */}
            <section className="dib relative w-100 gray lh-body f5 bg-transparent tl" id="home">
              <div className="db center mw8 ph4 pt6 pb4">
                <div className="dib relative w-100">
                  <h1 className={HeadingClass}>{home.title}</h1>
                  <div className={ParagraphClass}>
                    {home._rawBody ? <BlockContent blocks={home._rawBody} /> : null}
                  </div>
                  {/* <AnchorLink
                    className="dib relative w-100 w-auto-ns link dim ph4 pv3 b f5 bg-brand-navy white br3 mv0 tc"
                    href="#careers"
                  >
                    View roles
                  </AnchorLink> */}
                </div>
                <img
                  className="dib relative w-100 main-image"
                  src={homeImage}
                  alt={home.title}
                />
              </div>
            </section>

            {/* Story */}
            <section className="dib relative w-100 gray lh-body f5 tl" id="story">
              <div className="db center mw8 pa4">
                <h2 className={`${SubHeadingClass} brand-navy`}>{home.story}</h2>
                {/* <p className={`${ParagraphClass} preline`}>{home.storyBody}</p> */}
                <div className={`${ParagraphClass} preline`}>
                  <ReadMoreReact
                    readMoreText={'Read More'}
                    ideal={330}
                    text={home.storyBody}
                    min={100}
                    max={343}
                  />
                </div>
              </div>
            </section>

            {/* Values */}
            <section
              className="dib relative bg-brand-near-white w-100 gray lh-body f5 tl"
              id="values"
            >
              <div className="db center mw8 pa4">
                <h2 className={`${SubHeadingClass} brand-navy`}>{home.values}</h2>
                <p className={`${ParagraphClass} preline`}>{home.valuesBody}</p>
                {home.valueLI.map((object, index) => (
                  <div className="dib relative w-100 measure-wide" key={`value-${index}`}>
                    <ListItem object={object} />
                  </div>
                ))}
              </div>
            </section>

            {/* Equality */}
            <section
              className="dib relative w-100 white lh-body f5 tl overlay cover"
              style={{
                background: `url(${imageUrlFor(
                  buildImageObj(home.equalityImage)
                )}) no-repeat center center`
              }}
              id="equality"
            >
              <div className="db center mw8 ph4 pv6 pv7-l relative z-1">
                <h3 className={SubHeadingClass}>{home.equality}</h3>
                <p className={`${ParagraphClass} preline`}>{home.equalityBody}</p>
              </div>
            </section>

            {/* Benefits */}
            <section className="dib relative w-100 gray bg-white lh-body f5 tl" id="benefits">
              <div className="db center mw8 ph4 pt4">
                <div className="dib relative w-100 w-60-l">
                  <h4 className={HeadingClass2}>
                    {home.benefits}{' '}
                    {home.showBlue ? <span className="brand-blue">{home.benefitsBlue}</span> : null}
                  </h4>
                </div>
                <div className="dib relative w-100">
                  <div className="row top-xs">
                    {home.benefitsLI.map((object, index) => (
                      <div className="col-xs-12 col-md-6" key={Math.random()}>
                        <ListItem object={object} />
                      </div>
                    ))}
                  </div>
                </div>
                {home.image && home.image.asset && home.image.asset._id ? (
                  <div className="dib relative w-100 mt2 mt4-l">
                    <img
                      className="dib relative w-100 mt4"
                      src={imageUrlFor(buildImageObj(home.benefitsImage))}
                      alt={home.benefits}
                    />
                  </div>
                ) : null}
              </div>
            </section>

            {/* Careers */}
            <section className="dib relative w-100 gray lh-body f5 tl" id="careers">
              <div className="db center mw8 ph4 pb4 pb6-l pt4">
                <HR />
                <h5 className={`${SubHeadingClass} brand-navy`}>{home.careers}</h5>
                <p className={`${ParagraphClass} preline`}>{home.careersBody}</p>
                {home.showGPTW ? (
                  <div className="dib relative w-100 mt2 mb4">
                    <a
                      className="flex justify-start link dim inherit middle-xs"
                      href={home.gptwLink}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <img
                        className="dib relative h4 mr4"
                        src="/img/gptw.png"
                        alt="Great place to work certified"
                      />
                      <p className="measure-wide f6 ma0">{home.gptwBody}</p>
                    </a>
                  </div>
                ) : null}
                {loaded ? (
                  <div className="dib relative w-100">
                    <div className="dib relative w-100 measure-wide">
                      {jobs.map(job => (
                        <JobOpening job={job} key={job.id} />
                      ))}
                    </div>
                  </div>
                ) : null}
                <div className="dib relative w-100 mt2">
                  <a
                    className="dib relative w-100 w-auto-ns link dim ph4 pv3 b f5 bg-brand-green white br3 mv3 tc"
                    target="_blank"
                    href={home.careersLink}
                    rel="noopener noreferrer"
                  >
                    {home.careersCTA}
                  </a>
                  <p className="mb4 measure-wide f7 preline">{home.careersThanks}</p>
                </div>
              </div>
            </section>
          </article>
        </Layout>
      )
    }
  }
}

export default IndexPage

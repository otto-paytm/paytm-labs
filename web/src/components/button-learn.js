import React from 'react'
import { Link } from 'gatsby'

const ButtonClass =
  'dib relative w-100 w-auto-l tc b ph3 pv2 f6 bg-transparent black ba b--black bw2 link dim pointer'

class ButtonLearnMore extends React.Component {
  render() {
    const { external, dummy, text, to } = this.props

    const Arrow = () => (
      <img className="dib relative v-mid h2 fit ml3 bounce-h" src="/icons/arrow.svg" alt="" />
    )

    return (
      <>
        {external ? (
          <a className={ButtonClass} target="_blank" href={to} rel="noopener noreferrer">
            {text || `Learn more`}
            <Arrow />
          </a>
        ) : dummy ? (
          <button className={ButtonClass} type="button">
            {text || `Learn more`}
          </button>
        ) : (
          <Link className={ButtonClass} to={to}>
            {text || `Learn more`}
            <Arrow />
          </Link>
        )}
      </>
    )
  }
}

export default ButtonLearnMore

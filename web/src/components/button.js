import React from 'react'
import { Link } from 'gatsby'

const ButtonClass = 'dib relative w-100 w-auto-l tc b ph3 pv2 f6 bg-black white link dim pointer'

class Button extends React.Component {
  render() {
    const { external, type, text, to } = this.props

    return (
      <>
        {external ? (
          <a className={ButtonClass} target="_blank" href={to} rel="noopener noreferrer">
            {text || 'Learn more'}
          </a>
        ) : type ? (
          <button className={ButtonClass} type={type}>
            {text || 'Learn more'}
          </button>
        ) : (
          <Link className={ButtonClass} to={to}>
            {text || 'Learn more'}
          </Link>
        )}
      </>
    )
  }
}

export default Button

import React from 'react'

import { logEvent } from '../utils/analytics'

const footerLinks = 'dib relative link dim inherit v-mid f7 mr3 mr0-l ml3-l socials'
const imageClass = 'dib relative w-100 h-100 fit v-mid'
const year = new Date().getFullYear()

const Footer = class extends React.Component {
  constructor(props) {
    super(props)

    this.logClick = this.logClick.bind(this)
    this.getTime = this.getTime.bind(this)
  }

  logClick(name) {
    const event = {
      category: 'Social',
      action: `Clicked ${name}`
    }
    logEvent(event)
  }

  getTime(currentTime = new Date()) {
    const currentHour = currentTime.getHours()
    const splitAfternoon = 12 // 24hr time to split the afternoon
    const splitEvening = 17 // 24hr time to split the evening

    if (currentHour >= splitAfternoon && currentHour <= splitEvening) {
      // Between 12 PM and 5PM
      return 'Good afternoon'
    } else if (currentHour >= splitEvening) {
      // Between 5PM and Midnight
      return 'Good evening'
    }
    // Between dawn and noon
    return 'Good morning'
  }

  render() {
    const { address, email, name, siteTitle } = this.props

    return (
      <footer className="dib relative w-100 bg-white dark-gray lh-body f6 bt b--black-10 footer">
        <div className="db center ph4 pv4 mw8">
          {/* Top line */}
          <div className="dib flex-l justify-between w-100 items-center mb4">
            {/* Logo */}
            <div className="dib relative v-mid w-100 w-20-l mb4 mb0-l">
              <img className="dib relative logo v-mid" src="/img/logo.svg" alt={siteTitle} />
            </div>
            {/* Links */}
            <div className="div relative v-mid w-100 w-80-l tl tr-l">
              <a
                className={footerLinks}
                onClick={() => this.logClick('Facebook')}
                target="_blank"
                href="https://facebook.com/PaytmCanada/"
                rel="noopener noreferrer"
              >
                <img className={imageClass} alt="Facebook" src="/icons/fb.svg" />
              </a>
              <a
                className={footerLinks}
                onClick={() => this.logClick('Twitter')}
                target="_blank"
                href="https://twitter.com/paytmcanada"
                rel="noopener noreferrer"
              >
                <img className={imageClass} alt="Twitter" src="/icons/tw.svg" />
              </a>
              <a
                className={footerLinks}
                onClick={() => this.logClick('LinkedIn')}
                target="_blank"
                href="https://www.linkedin.com/company/paytm-labs/"
                rel="noopener noreferrer"
              >
                <img className={imageClass} alt="LinkedIn" src="/icons/ln.svg" />
              </a>
              <a
                className={footerLinks}
                onClick={() => this.logClick('Instagram')}
                target="_blank"
                href="https://www.instagram.com/paytm_canada/"
                rel="noopener noreferrer"
              >
                <img className={imageClass} alt="Instagram" src="/icons/ig.svg" />
              </a>
              <a
                className={footerLinks}
                onClick={() => this.logClick('Email')}
                target="_blank"
                href={`mailto:${email}`}
                rel="noopener noreferrer"
              >
                <img className={imageClass} alt="Email" src="/icons/mail.svg" />
              </a>
            </div>
          </div>
          {/* Bottom line */}
          <div className="dib flex-l justify-between w-100 items-top">
            <div className="dib relative w-100 tl">
              {/* HR */}
              <p className="dib relative w-100 fw4 f7 mv0">
                &copy; {year} {name}
                <a
                  className="dib relative w-100 link dim inherit mt2"
                  target="_blank"
                  href="https://paytm.ca/about"
                  rel="noopener noreferrer"
                >
                  {this.getTime()} from {address}
                </a>
              </p>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}

export default Footer

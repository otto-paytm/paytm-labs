import React from 'react'
import { Link } from 'gatsby'

const ButtonClass =
  'dib relative w-100 bg-transparent ba bw1 b--white white ph3 pv2 b f7 tc link dim br2'

class Banner extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isBottom: false,
      closed: props.closed || false
    }

    this.isBottomChecker = this.isBottomChecker.bind(this)
    this.toggleBanner = this.toggleBanner.bind(this)
  }

  isBottomChecker() {
    const isBottom = window.innerHeight + window.scrollY + 100 >= document.body.offsetHeight
    if (isBottom !== this.state.isBottom) {
      this.setState({ isBottom })
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.isBottomChecker)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.isBottomChecker)
  }

  toggleBanner() {
    this.setState(prevState => ({
      closed: !prevState.closed
    }))
  }

  render() {
    const { isBottom, closed } = this.state
    const { data } = this.props

    return (
      <div
        className={`banner dib fixed w-100 bottom-0 bg-brand-navy white z-2 ${
          isBottom || closed ? 'closed' : 'open'
        }`}
      >
        <button className="absolute close br-pill bg-white pa2 h2 w2 link dim pointer" onClick={this.toggleBanner}>
          <img src="/icons/close.svg" alt="Close" />
        </button>
        <div className="db center mw8 ph3 ph4-ns pv3">
          <div className="dib flex-ns items-center justify-between">
            {/* Text */}
            <div className="dib relative w-100 w-80-ns">
              <strong className="dib relative w-100 b f5 mb2">{data.text}</strong>
              {data.description ? (
                <p className="dib relative w-100 fw4 f7 o-80 mb4 mb0-ns">{data.description}</p>
              ) : null}
            </div>
            {/* CTA */}
            <div className="dib relative w-100 w-20-ns tc tr-ns">
              {data.external ? (
                <a
                  className={ButtonClass}
                  href={data.ctaLink}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {data.ctaText}
                </a>
              ) : (
                <Link className={ButtonClass} to={data.ctaLink}>
                  {data.ctaText}
                </Link>
              )}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Banner

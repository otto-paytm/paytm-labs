import React from 'react'

import { buildImageObj } from '../lib/helpers'
import { imageUrlFor } from '../lib/image-url'

import HR from './hr'

class Quote extends React.Component {
  render() {
    const { fullWidth, divider, quote } = this.props

    return (
      <div className={`${fullWidth ? 'col-xs-12' : 'col-xs-12 col-md-4'}`} key={Math.random()}>
        <div className="dib relative w-100 mb4 mb0-l">
          <div className="dib relative w-100">
            <strong className="dib relative w-100 f4 b mb3">{quote.title}</strong>
            <p className={`dib relative w-100 f6 gray ${divider ? 'mt0 mb0' : 'mt0 mb4'}`}>
              {quote.description}
            </p>
            {(quote.image && !quote.link) || quote.icon ? (
              <a
                className="dib relative w-100 link dim inherit b f6 mt4"
                target="_blank"
                href={quote.link}
                rel="noopener noreferrer"
              >
                <img
                  className="dib relative h2 fit"
                  src={imageUrlFor(buildImageObj(quote.image || quote.icon))}
                  alt={quote.title}
                />
              </a>
            ) : null}
            {divider ? <HR /> : null}
          </div>
        </div>
      </div>
    )
  }
}

export default Quote

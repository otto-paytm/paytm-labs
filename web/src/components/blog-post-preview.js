import React from 'react'
import { Link } from 'gatsby'

import { buildImageObj, getBlogUrl } from '../lib/helpers'
import { imageUrlFor } from '../lib/image-url'

const HeadingClass = 'dib relative w-100 f4 f3-ns b ma0 brand-navy'
const ParagraphClass = 'dib relative w-100 measure-wide inherit fw4 mb4 preline'

function BlogPostPreview(props) {
  const haveImage = props.mainImage && props.mainImage.asset && props.mainImage.asset._id

  return (
    <Link
      className={`dib relative w-100 h-blog overflow-hidden lh-body mb4 mb2-ns link dim ${
        haveImage ? 'bg-white black' : 'bg-black white pa4'
      }`}
      to={getBlogUrl(props.publishedAt, props.slug.current)}
    >
      <div className="dib relative w-100">
        {haveImage && (
          <img
            className="dib relative w-100 fit"
            src={imageUrlFor(buildImageObj(props.mainImage))
              .width(600)
              .height(Math.floor((9 / 16) * 600))
              .url()}
            alt={props.mainImage.alt}
          />
        )}
        {props.categories && props.categories.length > 0 ? (
          <small className="dib relative w-100 ttu f7 tracked b">{props.categories[0].title}</small>
        ) : null}
        <h3
          className={HeadingClass}
          style={{
            marginBottom: '-2rem'
          }}
        >
          {props.title}
        </h3>
        {props.description ? (
          <p className={ParagraphClass}>
            {props.description}
          </p>
        ) : null}
      </div>
    </Link>
  )
}

export default BlogPostPreview

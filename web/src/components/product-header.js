import React from 'react'

// Libs
import { buildImageObj } from '../lib/helpers'
import { imageUrlFor } from '../lib/image-url'

// Components
import ButtonLearn from './button-learn'
import HR from './hr'

class ProductHeader extends React.Component {
  constructor(props) {
    super(props)

    this.handleBack = this.handleBack.bind(this)
  }

  handleBack() {
    window.history.back()
  }

  render() {
    const { align, back, button, object } = this.props

    return (
      <div className="dt relative vh-100 w-100 lh-body product-main">
        <div className={`dtc w-100 h-100 relative ${align || 'v-btm'}`}>
          <div className="db center mw8 ph4">
            {back ? (
              <button
                className="dib absolute left-0 top-0 w2 w3-l h2 h3-l ml3 ml4-l mt5 mt6-l pa0 pa2-l link dim pointer bg-transparent bn"
                onClick={() => this.handleBack()}
              >
                <img className="dib relative w-100 h-100 fit" src="/icons/back.svg" />
              </button>
            ) : null}
            <div className="dib relative w-100 w-60-l">
              {object.icon && object.icon.asset && object.icon.asset._id ? (
                <div className="dib relative w-100 mb4">
                  <img
                    className="w3 h3 fit"
                    alt={object.title}
                    src={imageUrlFor(buildImageObj(object.icon))}
                  />
                </div>
              ) : null}
              <h1 className="dib relative w-100 f2 f1-ns f-headline-l inherit mv0 lh-title">
                {object.title}
                <sup className="ml3 f4 absolute top-0">AI</sup>
              </h1>
              <HR fullWidth thick />
              <p
                className={`dib relative w-100 f5 f4-l fw4 o-80 mt0 mt2-l ${
                  button ? 'mb4' : 'mb5 mb6-l'
                }`}
              >
                {object.description}
              </p>
              {button && object.slug.current ? (
                <div className="dib relative w-100 mb4 mb5-l">
                  <ButtonLearn text="Learn more" to={`/products/${object.slug.current}`} dummy />
                </div>
              ) : null}
              {object.listItems ? (
                <div className="dib relative w-100 mb4 mb5-l">
                  <div className="row">
                    {object.listItems.map((item, index) => (
                      <div className="col-xs-6 col-md-3" key={`item-${index}`}>
                        <div className="dib relative w-100 mb3 mb0-l">
                          <strong className="dib relative w-100 mb2 f6 f5-l">{item.title}</strong>
                          <p className="dib relative w-100 f6 gray ma0 pa0">{item.description}</p>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              ) : null}
            </div>
          </div>
        </div>
        {object.mainImage && object.mainImage.asset._id ? (
          <img
            className="dn dib-l absolute right-0 top-0 w6 vh-100 fit"
            alt={object.title}
            src={imageUrlFor(buildImageObj(object.mainImage))}
          />
        ) : null}
      </div>
    )
  }
}

export default ProductHeader

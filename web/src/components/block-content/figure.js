import React from 'react'

import { buildImageObj } from '../../lib/helpers'
import { imageUrlFor } from '../../lib/image-url'

function Figure(props) {
  return (
    <figure className="dib relative w-100 mh0 mv4">
      {props.asset && (
        <img
          src={imageUrlFor(buildImageObj(props))
            .width(1200)
            .url()}
          alt={props.alt}
        />
      )}
      <figcaption className="dib relative w-100 measure-wide inherit f6 fw4 mb4 preline dark-gray">{props.caption}</figcaption>
    </figure>
  )
}

export default Figure

import React from 'react'

const Divider = () => (
  <span className="dn dib-l relative mh3 v-mid">
    /
  </span>
)

export default Divider

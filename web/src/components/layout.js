import React from 'react'
// import CookieConsent from 'react-cookie-consent'

import { initGA, logPageView } from '../utils/analytics'

import Header from './header'
import Footer from './footer'

import 'flexboxgrid'
import 'tachyons'

import '../styles/all.scss'

class Layout extends React.Component {
  componentDidMount() {
    if (!window.GA_INITIALIZED) {
      initGA()
      window.GA_INITIALIZED = true
    }
    logPageView()
  }

  render() {
    const { children, companyInfo, siteTitle, home } = this.props

    return (
      <>
        <Header siteTitle={siteTitle} email={companyInfo.email} home={home} />
        {/* <CookieConsent
          enableDeclineButton
          flipButtons
          buttonText="Got it"
          cookieName="paytm-labs"
          location="bottom"
          style={{ background: '#000000' }}
          declineButtonClasses="link dim"
          buttonClasses="link dim"
          declineButtonStyle={{
            fontWeight: '600',
            background: 'none',
            border: '2px solid #ccc',
            color: '#ccc'
          }}
          buttonStyle={{
            fontWeight: '600',
            background: 'white',
            border: '2px solid white',
            color: 'black'
          }}
          expires={150}
        >
          This website uses cookies to improve your experience. You may opt-out if you wish.
        </CookieConsent> */}
        {children}
        <Footer
          addressLineTwo={companyInfo.address2}
          address={companyInfo.address1}
          city={companyInfo.city}
          country={companyInfo.country}
          email={companyInfo.email}
          name={companyInfo.name}
          siteTitle={siteTitle}
        />
      </>
    )
  }
}

export default Layout

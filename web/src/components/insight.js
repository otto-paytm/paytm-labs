import React from 'react'
import { format, distanceInWords, differenceInDays } from 'date-fns'

import { buildImageObj } from '../lib/helpers'
import { imageUrlFor } from '../lib/image-url'

import BlockContent from './block-content'

const ParagraphClass = 'dib relative w-100 measure-wide dark-gray fw4 mb4'
const HeadingClass = 'dib relative w-100 measure f2 b mv3 black'

function Insight(props) {
  const { _rawBody2, _rawBody, title, publishedAt, description, mainImage } = props

  return (
    <article>
      <section className="dib relative w-100 bg-white lh-body dark-gray f5">
        <div className="db center mw8 ph4 pv6">
          <div className="row">
            <div className="col-xs-12 col-md-7">
              <div className="dib relative w-100 pr0 pr4-l">
                <h1 className={HeadingClass}>{title}</h1>
                {description ? <p className={ParagraphClass}>{description}</p> : null}
                {publishedAt ? (
                  <small className="dib relative w-100 f6 gray">
                    Published:{' '}
                    {differenceInDays(new Date(publishedAt), new Date()) > 3
                      ? distanceInWords(new Date(publishedAt), new Date())
                      : format(new Date(publishedAt), 'MMMM Do YYYY')}
                  </small>
                ) : null}
                {_rawBody ? <BlockContent blocks={_rawBody} /> : null}
              </div>
            </div>
            <div className="col-xs-12 col-md-5">
              {mainImage ? (
                <img
                  className="dib relative w-100 br3"
                  src={imageUrlFor(buildImageObj(mainImage))
                    .width(1200)
                    .height(Math.floor((9 / 16) * 1200))
                    .fit('crop')
                    .url()}
                  alt={title}
                />
              ) : null}
              {_rawBody2 ? <BlockContent blocks={_rawBody2} /> : null}
            </div>
          </div>
        </div>
      </section>
    </article>
  )
}

export default Insight

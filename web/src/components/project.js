import React from 'react'

import { buildImageObj } from '../lib/helpers'
import { imageUrlFor } from '../lib/image-url'

import ProjectPreviewGrid from './project-preview-grid'
import ProductHeader from './product-header'
import BlockContent from './block-content'
import Sales from './sales'
import HR from './hr'

const ParagraphClass = 'dib relative w-100 measure-wide inherit fw4 mb4'
const HeadingClass = 'dib relative w-100 measure-wide f2 b mv3 inherit'

function Project(props) {
  return (
    <article id="product">
      {/* Start */}
      <section className="dt relative vh-100 w-100">
        <ProductHeader object={props} back />
      </section>
      {/* Content */}
      <section className="dib relative w-100 black lh-body f5">
        <div className="db center mw8 ph4 pv4 pv6-l">
          <div className="dib relative w-100 bg-brand-near-white-l pa0 pa6-l">
            <HR />
            {props.title2 ? <h2 className={HeadingClass}>{props.title2}</h2> : null}
            {props._rawBody ? (
              <div className={ParagraphClass}>
                <BlockContent blocks={props._rawBody} />
              </div>
            ) : null}
          </div>
          {props.secondImage && props.secondImage.asset && props.secondImage.asset._id ? (
            <div className="dib relative w-100">
              <img
                className="dib relative w-100 h-100 subheader fit"
                alt={props.title2}
                src={imageUrlFor(buildImageObj(props.secondImage))}
              />
            </div>
          ) : null}
          {props.valueItems && props.valueItems.length > 0 ? (
            <div className="row items-top">
              {props.valueItems.map((object, index) => (
                <div className="col-xs-12 col-md-4" key={`val-${index}`}>
                  <div className="dib relative w-100">
                    <HR />
                    <strong className="dib relative w-100 f4 b mb3">{object.title}</strong>
                    <p className="dib relative w-100 ma0 f6 gray">{object.description}</p>
                  </div>
                </div>
              ))}
            </div>
          ) : null}
        </div>
      </section>
      {/* Sales */}
      <section className="dib relative w-100 dark-gray lh-body f5 bg-white overflow-hidden bt b--black-10">
        <div className="db center mw8 ph4 pv6">
          <Sales
            title={props.sales.title}
            image={props.sales.image}
            _rawBody={props.sales._rawBody}
          />
        </div>
      </section>
      {/* Related products */}
      {props.relatedProducts && props.relatedProducts.length > 0 ? (
        <section className="dib relative w-100 white lh-body f5 bg-dark-gray">
          <div className="db center mw8 ph4 pv4 pv6-l">
            <h3 className={HeadingClass}>Related products</h3>
            <ProjectPreviewGrid nodes={props.relatedProducts} />
          </div>
        </section>
      ) : null}
    </article>
  )
}

export default Project

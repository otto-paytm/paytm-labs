import React from 'react'
import AnchorLink from 'react-anchor-link-smooth-scroll'
import { Link } from 'gatsby'

const linkClass =
  'bg-transparent bn link dim dib relative w-100 w-auto-l inherit pa2 pv0-l ph0 ph2-l tl tr-l pointer mb3 mb0-ns'
const footerLinks = 'dib relative link dim inherit v-mid f7 mr3 mr0-l ml3-l socials'
const imageClass = 'dib relative w-100 h-100 fit v-mid'

const Header = class extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      menuOpen: false,
      isTop: true
    }

    this.isTopChecker = this.isTopChecker.bind(this)
    this.toggleMenu = this.toggleMenu.bind(this)
    this.closeMenu = this.closeMenu.bind(this)
  }

  isTopChecker() {
    const isTop = window.scrollY < 200
    if (isTop !== this.state.isTop) {
      this.setState({ isTop })
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.isTopChecker)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.isTopChecker)
  }

  toggleMenu() {
    this.setState({ menuOpen: !this.state.menuOpen })
  }

  closeMenu() {
    if (this.state.menuOpen) {
      this.setState({
        menuOpen: false
      })
    }
  }

  render() {
    const { siteTitle, email, home } = this.props

    const Logo = () => {
      return home ? (
        <AnchorLink className="dib relative link dim tl z-1 inherit" title={siteTitle} href="#home">
          <img className="dib relative v-mid logo mr3" alt={siteTitle} src="/img/logo.svg" />
        </AnchorLink>
      ) : (
        <Link className="dib relative link dim tl z-1 inherit" title={siteTitle} to="/">
          <img className="dib relative v-mid logo mr3" alt={siteTitle} src="/img/logo.svg" />
        </Link>
      )
    }

    const RightNav = () => (
      <div className="dib relative w-100 tl tc-l f4 f6-ns">
        {home ? (
          <>
            <AnchorLink className={linkClass} onClick={this.closeMenu} href="#values">
              Values
            </AnchorLink>
            <AnchorLink className={linkClass} onClick={this.closeMenu} href="#benefits">
              Benefits
            </AnchorLink>
            <Link className={linkClass} onClick={this.closeMenu} to="/blog">
              Blog
            </Link>
            <AnchorLink
              className="dib relative v-base ba bw1 gradient-button brand-navy link dim ph3 pv2 br3 b ml0 ml2-ns w-100 w-auto-ns tc"
              onClick={this.closeMenu}
              href="#careers"
            >
              We're hiring
            </AnchorLink>
          </>
        ) : (
          <>
            <Link className={linkClass} onClick={this.closeMenu} to="/#values">
              Values
            </Link>
            <Link className={linkClass} onClick={this.closeMenu} to="/#benefits">
              Benefits
            </Link>
            <Link className={linkClass} onClick={this.closeMenu} to="/blog">
              Blog
            </Link>
            <Link
              className="dib relative v-base ba bw1 gradient-button brand-navy link dim ph3 pv2 br3 b ml0 ml2-ns w-100 w-auto-ns tc"
              onClick={this.closeMenu}
              to="/#careers"
            >
              We're hiring
            </Link>
          </>
        )}
      </div>
    )

    const CloseButton = () => (
      <button
        className={`bg-transparent bn link dim dib dn-l relative w-100 w-auto-l inherit f7 ttu b`}
        onClick={this.closeMenu}
      >
        Close
      </button>
    )

    const MenuButton = () => (
      <button
        className="dib dn-l relative bn bg-transparent w-auto inherit f7 ttu b"
        onClick={this.toggleMenu}
      >
        Menu
      </button>
    )

    return (
      <nav
        aria-label="main-navigation"
        className={`navbar w-100 fixed left-0 top-0 f5 z-2 black bg-white bb ph4 ph0-ns ${
          !this.state.isTop ? 'b--black-10 pv3' : 'b--white pv3 pv4-l'
        }`}
        role="navigation"
      >
        {/* Mobile nav */}
        <div
          className={`dib bg-white fixed overflow-y-scroll left-0 top-0 z-3 w-100 h-100 mobile pv3 ph4 ${
            this.state.menuOpen ? 'open' : ''
          }`}
        >
          <div className="flex w-100 justify-between items-center">
            <Logo />
            <div className="dib dn-l relative tr">
              <CloseButton />
            </div>
          </div>
          <div className="dib relative w-100 mv4 f3">
            <RightNav />
          </div>
          <div className="dib relative w-100 absolute bottom-0 left-0 pa4">
            <div className="div relative v-mid w-100 w-80-l tl tr-l">
              <a
                className={footerLinks}
                onClick={() => this.logClick('Facebook')}
                target="_blank"
                href="https://facebook.com/PaytmCanada/"
                rel="noopener noreferrer"
              >
                <img className={imageClass} alt="Facebook" src="/icons/fb.svg" />
              </a>
              <a
                className={footerLinks}
                onClick={() => this.logClick('Twitter')}
                target="_blank"
                href="https://twitter.com/paytmcanada"
                rel="noopener noreferrer"
              >
                <img className={imageClass} alt="Twitter" src="/icons/tw.svg" />
              </a>
              <a
                className={footerLinks}
                onClick={() => this.logClick('LinkedIn')}
                target="_blank"
                href="https://www.linkedin.com/company/paytm-labs/"
                rel="noopener noreferrer"
              >
                <img className={imageClass} alt="LinkedIn" src="/icons/ln.svg" />
              </a>
              <a
                className={footerLinks}
                onClick={() => this.logClick('Instagram')}
                target="_blank"
                href="https://www.instagram.com/paytm_canada/"
                rel="noopener noreferrer"
              >
                <img className={imageClass} alt="Instagram" src="/icons/ig.svg" />
              </a>
              <a
                className={footerLinks}
                onClick={() => this.logClick('Email')}
                target="_blank"
                href={`mailto:${email}`}
                rel="noopener noreferrer"
              >
                <img className={imageClass} alt="Email" src="/icons/mail.svg" />
              </a>
            </div>
          </div>
        </div>
        {/* Non-mobile nav */}
        <div
          className={`db center mw8 ph0 ph4-l ${
            typeof window !== 'undefined' && window.location.pathname !== '/' ? 'black' : ''
          }`}
        >
          <div className="flex w-100 justify-between items-center">
            {/* Left */}
            <Logo />
            {/* Right */}
            <div className="dn dib-l relative tr">
              <RightNav />
            </div>
            {/* Menu */}
            <div className="dib dn-l relative tr">
              <MenuButton />
            </div>
          </div>
        </div>
      </nav>
    )
  }
}

export default Header

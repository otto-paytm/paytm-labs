import React from 'react'
import { graphql } from 'gatsby'

import { buildImageObj, getBlogUrl } from '../lib/helpers'
import { imageUrlFor } from '../lib/image-url'

import GraphQLErrorList from '../components/graphql-error-list'
import Container from '../components/container'
import BlogPost from '../components/blog-post'
import SEO from '../components/seo'

import Layout from '../containers/layout'

export const query = graphql`
  query BlogPostTemplateQuery($id: String!) {
    post: sanityPost(id: { eq: $id }) {
      id
      publishedAt
      categories {
        _id
        title
      }
      mainImage {
        crop {
          _key
          _type
          top
          bottom
          left
          right
        }
        hotspot {
          _key
          _type
          x
          y
          height
          width
        }
        asset {
          _id
        }
        alt
      }
      title
      slug {
        current
      }
      description
      _rawBody
    }
  }
`

const BlogPostTemplate = props => {
  const { data, errors } = props
  const post = data && data.post

  return (
    <Layout>
      {errors && <SEO title="GraphQL Error" />}
      {post && (
        <SEO
          title={post.title || 'Untitled'}
          description={post.description || null}
          image={
            post.mainImage
              ? imageUrlFor(buildImageObj(post.mainImage)).url()
              : 'https://paytmlabs.com/img/social.png'
          }
          url={
            post.publishedAt && post.slug.current
              ? `https://paytmlabs.com${getBlogUrl(post.publishedAt, post.slug.current)}`
              : 'https://paytmlabs.com/blog'
          }
        />
      )}

      {errors && (
        <Container>
          <GraphQLErrorList errors={errors} />
        </Container>
      )}

      {post && <BlogPost {...post} />}
    </Layout>
  )
}

export default BlogPostTemplate

import React from 'react'
import { graphql, Link } from 'gatsby'
import { format, distanceInWords, differenceInDays } from 'date-fns'

import {
  filterOutDocsWithoutSlugs,
  filterOutDocsPublishedInTheFuture,
  mapEdgesToNodes,
  buildImageObj,
  getBlogUrl
} from '../lib/helpers'

import { imageUrlFor } from '../lib/image-url'

import GraphQLErrorList from '../components/graphql-error-list'
import BlockContent from '../components/block-content'
import Container from '../components/container'
import SEO from '../components/seo'

import Layout from '../containers/layout'

const ParagraphClass = 'dib relative w-100 gray fw4'
const HeadingClass = 'dib relative w-100 measure-wide f2 b mv3 black'
const TabClass = 'dib relative w-auto b f7 ttu black bb bw1 mr3 mr4-ns link dim pv2 tracked'

export const query = graphql`
  query TopicPostTemplateQuery($title: String!, $slug: String!) {
    topic: sanityCategory(title: { eq: $title }) {
      id
      title
    }
    posts: allSanityPost(
      filter: { categories: { elemMatch: { slug: { current: { eq: $slug } } } } }
    ) {
      edges {
        node {
          id
          title
          description
          publishedAt
          slug {
            current
          }
          mainImage {
            asset {
              _id
            }
            alt
          }
        }
      }
    }
  }
`

const TopicTemplate = props => {
  const { data, errors } = props
  const topic = data && data.topic
  const posts = (data || {}).posts
    ? mapEdgesToNodes(data.posts)
        .filter(filterOutDocsWithoutSlugs)
        .filter(filterOutDocsPublishedInTheFuture)
    : []

  return (
    <Layout>
      {errors && <SEO title="GraphQL Error" />}
      {topic && <SEO title={topic.title || 'Untitled'} />}

      {errors && (
        <Container>
          <GraphQLErrorList errors={errors} />
        </Container>
      )}

      {topic && (
        <article>
          <section className="dib relative w-100 lh-body dark-gray f5 post">
            <div className="db center mw8 ph4 pv6">
              <h1 className={HeadingClass}>{topic.title || `Topic`}</h1>
              <div className="dib relative w-100 overflow-x-auto mb4 nowrap">
                <Link className={`${TabClass} b--white`} to="/blog">
                  Latest posts
                </Link>
                <Link className={`${TabClass} b--white`} to="/topics">
                  Topics
                </Link>
              </div>
              {posts && posts.length > 0 ? (
                posts.map((object, index) => (
                  <Link
                    className="dib relative w-100 link dim pv3 pv4-ns bb b--black-10"
                    key={`post-${index}`}
                    to={getBlogUrl(object.publishedAt, object.slug.current)}
                  >
                    <div className="row items-center">
                      <div className="col-xs-12 col-md-8">
                        <strong className="dib relative w-100 f4 f3-ns b brand-navy">
                          {object.title}
                        </strong>
                        {object.publishedAt && (
                          <p className={`${ParagraphClass} f7 o-60`}>
                            Posted:{' '}
                            {differenceInDays(new Date(object.publishedAt), new Date()) > 3
                              ? distanceInWords(new Date(object.publishedAt), new Date())
                              : format(new Date(object.publishedAt), 'MMMM Do YYYY')}
                          </p>
                        )}
                        {object.description ? <p className={ParagraphClass}>{object.description}</p> : null}
                      </div>
                      {object.mainImage && object.mainImage.asset ? (
                        <div className="dn dib-ns col-xs-12 col-md-4 tr">
                          <img
                            className="dib relative w-100 h6 scale"
                            src={imageUrlFor(buildImageObj(object.mainImage)).url()}
                            alt={object.mainImage.alt || `Blog post image`}
                          />
                        </div>
                      ) : null}
                    </div>
                  </Link>
                ))
              ) : (
                <div className="dib relative w-100 tc">
                  <img className="db center mw5 pa4" alt="Paytm Labs" src="/img/logo.svg" />
                  <h1 className={HeadingClass}>Not posts found</h1>
                  <p className={ParagraphClass}>
                    Oh no... we don't have any content on this page right now. Check back later!
                  </p>
                </div>
              )}
            </div>
          </section>
        </article>
      )}
    </Layout>
  )
}

export default TopicTemplate

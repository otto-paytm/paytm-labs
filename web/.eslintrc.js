const path = require('path')

module.exports = {
  extends: [
    'standard',
    'standard-react',
    'plugin:import/errors',
    'plugin:import/warnings'
  ],
  rules: {
    'space-before-function-paren': ['error', 'never'],
    'jsx-quotes': ['error', 'prefer-double'],
    quotes: [
      2,
      'single',
      {
        avoidEscape: true
      }
    ],
    'react/prop-types': 0,
    semi: ['error', 'never']
  },
  settings: {
    react: {
      pragma: 'React',
      version: '16.6.3'
    }
  }
}
